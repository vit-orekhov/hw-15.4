#include <iostream>
#include <string>
#include <iomanip>
#include <Windows.h>



int main()
{
	
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(handle, FOREGROUND_RED);

	std::cout << "Do you want to see odd or even numbers from 0 to N?" << '\n';
	
	HANDLE handle2 = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle2, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	std::cout << "Type 1 and press Enter if you want to see odd numbers" << '\n' << "Type 2 and press Enter if you want to see even numbers" << '\n';

	int oe = 0;

	std::cin >> oe;

	if (oe == 2)
	{

		std::cout << "Type upper limit number, N = ";
		int N = 0; //upper limit


		std::cin >> N;


		for (int m = 1; m <= N; ++m) //m - iteration turn
		{

			int g = 0; //odds
			g = m * 2;

			if (g <= N)
			{
				std::cout << g << '\n';
			}
		}

	}

	if (oe == 1)
	{

		std::cout << "Type upper limit number, N = ";
		int N = 0; //upper limit


		std::cin >> N;


		for (int m = 1; m <= N; ++m) //m - iteration turn
		{

			int g = 0; //odds
			g = ((m * 2) - 1);

			if (g <= N)
			{
				std::cout << g << '\n';
			}
		}

	}

}

